#include <iostream>
#include <stdlib.h>

inline int xor_regular(int a, int b)
{
    return a ^ b;
}

inline int xor_f(int a, int b)
{
    return (a | b) & ~(a & b);
}

inline int xor_nand(int a, int b)
{
    int a_b = ~(a & b);
    int l = ~(a_b & a);
    int r = ~(a_b & b);
    return ~(l & r);
}

int main(int argc, char** argv)
{
    if(argc != 3)
    {
        std::cerr << "You need to pass 2 arguments as A ^ B values." << std::endl;
        exit(EXIT_FAILURE);
    }

    int a = atoi(argv[1]);
    int b = atoi(argv[2]);

    std::cout << "Regular XOR: " << xor_regular(a, b) << std::endl;
    std::cout << "XOR F: " << xor_f(a, b) << std::endl;
    std::cout << "XOR NAND: " << xor_nand(a ,b) << std::endl;
    return 0;
}
